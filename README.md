# Awesome Cloud Certification 

## GOAL
The goal of this list is 
* to give an overview of the available Cloud Certifications. 
* Provinding them with some meta-data 
    * on content, 
    * how long they are valid, 
    * priceing, 
    * duration of study, and 
    * study content.

## Vallue of Certifications
The topic cloud is non-stop in movement, and certifications are not of very much vallue when working day-in-day-out in the cloud ecosystem. But for non-core cloud-engineers, architects etc certifications are a way to show they have some experience and knowledge.

## Contents (ToC or LoC)
- [Amazon - AWS](#amazon-aws)
    - [AWS - Levels of certification](#aws-levels-of-certification)
    - [AWS Certifications](#aws-certifications)
        - [AWS - Architect- Levels of certification](#aws-architect-levels-of-certification)
        - [AWS - Developer- Levels of certification](#aws-developer-levels-of-certification)
        - [AWS - Operations- Levels of certification](#aws-operations-levels-of-certification)
        - [AWS - Specialist- Levels of certification](#aws-specialist-levels-of-certification)
    - [AWS Training](#aws-training)
    - [AWS - Resources](#aws-resources)
- [Microsoft - Azure](#microsoft-azure)
    - [MTA: Microsoft Technology Associate](#mta-microsoft-technology-associate)
    - [MCSA: Microsoft Certified Solutions Associate](#msca-microsoft-certified-solutions-associate)
    - [MCSE: Microsoft Certified Solutions Expert](#mcse-microsoft-certified-solutions-expert)
    - [Microsoft-Azure-and-Cloud-Plaform-Certification](#microsoft-azure-and-cloud-plaform-certification)
- [Google - GCP](#google-gcp)
    -  [GCP - Resources](#gcp-resources)
-  [Cloud Security Alliance (CSA)](#cloud-security-alliance-csa-and-isc)
- [Links](#links)
- [Contributing](#contributing)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)
    -  [Inspiration on Readme.md](#inspiration-on-readme.md)
    -  [Inspiration on Markdown Syntax](#inspiration-on-Markdown-syntax)

## Amazon - AWS
### [AWS - Levels of certification](https://aws.amazon.com/certification/)
1. **Foundational** - Validates overall understanding of the AWS Cloud. Prerequisite to achieving Specialty certification or an optional start towards Associate certification.
1. **Associate** - Technical role-based certifications. No prerequisite.
1. **Professional** - Highest level technical role-based certification. Relevant Associate certification required.

### AWS Certifications
#### [AWS - Architect- Levels of certification](https://aws.amazon.com/certification/)
1. Solution Architect - Professional
1. Solution Architect - Associate
1. [Cloud Practitioner - Foundational](https://aws.amazon.com/training/path-cloudpractitioner/)

#### [AWS - Developer- Levels of certification](https://aws.amazon.com/certification/)
1. DevOps Engineer - Professional
1. Developer - Associate
1. [Cloud Practitioner - Foundational](https://aws.amazon.com/training/path-cloudpractitioner/)

#### [AWS - Operations- Levels of certification](https://aws.amazon.com/certification/)
1. DevOops Engineer - Professional
1. SySOps Administrator - Associate
1. [Cloud Practitioner - Foundational](https://aws.amazon.com/training/path-cloudpractitioner/)

#### [AWS - Specialist- Levels of certification](https://aws.amazon.com/certification/)
1. Advanced Networking
1. Big Data
1. Security

### [AWS Training](https://www.aws.training/)
1. [Training Cloud Practitioner - Foundational](https://www.aws.training/learningobject/curriculum?id=16357&src=path-cp)
    1. duration : 7 hours
    2. form: video's (7 mandatory & 1 optional) video's with multiple choise questions at the end of the video

### AWS - Resources
1. [Amazon Partner Network](https://partnercentral.awspartner.com/)
1. [Amazon Partner Training](https://aws.amazon.com/partners/training/)
1. [Amazon Certification & Training](https://www.aws.training/)
1. [Example Certifications](https://www.certmetrics.com/amazon)

## [Microsoft - Azure](https://www.microsoft.com/en-gb/learning/certification-overview.aspx)
### MTA: Microsoft Technology Associate
1. Fundamental technical knowledge
1. Microsoft Technology Associate is an entry-level certification intended for people seeking knowledge of fundamental technology concepts. MTA certification addresses a wide spectrum of basic technical concepts, assesses and validates your core technical knowledge, and enhances your technical credibility.

### MCSA: Microsoft Certified Solutions Associate
1. Core skills for IT professionals and developers
1. Microsoft Certified Solutions Associate is intended for people who seek entry-level jobs in an information technology environment. MCSA is a prerequisite for more advanced Microsoft certifications.

### MCSE: Microsoft Certified Solutions Expert
1. The globally recognised standard for IT Professionals
1. Microsoft Certified Solutions Expert is a certification intended for IT professionals seeking to demonstrate their ability to build innovative solutions across multiple technologies, both on-premises and in the cloud.

### Microsoft [Azure](https://www.microsoft.com/en-gb/learning/browse-all-certifications.aspx?technology=Microsoft%20Azure) and [Cloud Plaform](https://www.microsoft.com/en-gb/learning/browse-all-certifications.aspx?technology=Cloud%20Computing) Certification
1. MTA: Certification
1. MCSA: Cloud Platform
1. MCSA: Linux on Azure
1. MCSE: Cloud Platform and Infrastructure

## [Google - GCP](https://cloud.google.com/certification/)
1. Associate Cloud Engineer
    1. Demonstrate your ability to deploy applications, monitor operations, and maintain cloud projects on Google Cloud Platform.
1. Professional Cloud Architect
    1. Demonstrate your proficiency to design, build and manage solutions on Google Cloud Platform.
1. Professional Data Engineer
    1. Demonstrate your proficiency to design and build data processing systems and create machine learning models on Google Cloud Platform.
1. G Suite Administrator
    1. Show your technical skills to manage a large scale G Suite domain.
1. G Suite
    1. Prove your expertise in using G Suite productivity and collaboration tools with this certification. Demonstrate your skills and advance your career.

###  GCP - Resources
1. [Github - Google Cloud Architect Professional Certification Exam - Preparation Resources](https://github.com/agasthik/GoogleCloudArchitectProfessional)
1. [GCP Learning Track - introduction to GCP](https://cloud.google.com/training/onramps)
1. [Coursera - GCP courses](https://www.coursera.org/googlecloud)
1. [GCP Partner Page](https://www.cloudconnect.goog/community/partners#manuallyInitiated)
1. [Awesomelist - Google Cloud Architect Professional Certification Exam - Preparation Resources](https://github.com/agasthik/GoogleCloudArchitectProfessional)

##  [Cloud Security Alliance (CSA)](https://cloudsecurityalliance.org/) and [(ISC)²](https://www.isc2.org/)
1. (ISC)² CCSP (Certified Cloud Security Professional) 

## Links
1. [firebrandtraining - Jan 2018 - 10 best Cloud Certifications](http://blog.firebrandtraining.co.uk/2018/01/10-best-cloud-certifications-2018.html)
    1. CompTIA Cloud+
      1. The CompTIA Cloud+ is a robust entry-level certification aimed at professionals with 1-2 years’ experience working with networks, storage or data centre admin. Not to be confused with CompTIA’s Cloud Essentials, which is designed as an introduction to cloud for non-IT professionals.
    1. (ISC)² CCSP
      1. This high-level qualification builds on knowledge gained from the renowned CISSP – holders of this cert are prequalified to sit the CCSP exam. Otherwise, you’ll need to satisfy rigorous prerequisites, including a minimum of five years full-time work experience in IT.
      1. You’ll demonstrate your expert knowledge of cloud application, platform and data infrastructure security. You’ll even focus on compliance, legality, audit processes and privacy – particularly relevant with the introduction of GDPR in May 2018.
    1. Microsoft MCSA: Cloud Platform
      1. Azure / Cloud Developer
        1. 70-532: Developing Microsoft Azure Solutions
        1. 70-473: Designing and Implementing Cloud Data Platform Solutions
      1. Azure / Cloud Infrastructure and Architecture
          1. 70-533: Implementing Microsoft Azure Infrastructure Solutions
          1. 70-534: Architecting Microsoft Azure Solutions
      1. Big Data Architect
          1. 70-534: Architecting Microsoft Azure Solutions
          1. 70-475: Designing and Implementing Big Data Analytics Solutions
      1. Azure / Cloud Data and Big Data
          1. 70-473: Designing and Implementing Cloud Data Platform Solutions
          1. 70-475: Designing and Implementing Big Data Analytics Solutions
      1. Azure / Cloud Big Data Developer
          1. 70-532: Developing Microsoft Azure Solutions
          1. 70-475: Designing and Implementing Big Data Analytics Solutions
    1. Microsoft MCSE: Cloud Platform and Infrastructure
        1. ?
    1. Microsoft MCSA: Linux on Azure
        1. Get certified and you’ll learn how to deploy, configure, host and manage Linux websites on Azure. You’ll also learn how to take advantage of Azure’s key features, including PowerShell, and study Azure Active Directory, virtual machines and virtual networks using Linux.
    1. AWS Certified Solutions Architect – Associate
        1. The first level of the Architecting track - AWS Certified Solutions Architect – Associate – is built for professionals that want to design and deploy apps and services using AWS. You’ll also learn how to make use of key cloud features, like scalability, high availability and geo-redundancy. 
        1. Despite being the first certification in the track, this is by no means entry-level. Before attempting the exam, AWS recommend you take two short virtual courses: Architecting on AWS and AWS Certification Exam Readiness Workshop: AWS Certified Solutions Architect - Associate.
    1. AWS Certified Solutions Architect – Professional
        1. You’ll prove your advanced technical skills and experience in designing distributed applications and systems. With this credential, you’ll work to design and build solutions that meet client AWS requirements, recommend architecture and provision AWS apps.
        1. The prerequisites are high – you’ll need to have two or more years’ hands-on experience with cloud architecture on AWS and the ability to provide best practice guidance on architecture across enterprises. Of course, you’ll also need to possess the Certified Solutions Architect – Associate certification.
    1. Google Professional Cloud Architect
        1. 
    1. Cisco CCNA Cloud
    1. Cisco CCNP Cloud

1. [Business New Daily - Top 5 cloud Certifications](https://www.businessnewsdaily.com/10748-top-5-cloud-certifications.html)
1. [IT Certification Master Site - Cloud](http://itcertificationmaster.com/it-certifications/cloud-certifications/)
    1. AWS
        1. AWS Certified Solutions Architect – Associate Level
        1. AWS Certified Solutions Architect – Professional Level
        1. AWS Certified Developer – Associate Level
        1. AWS Certified SysOps Administrator – Associate Level
        1. AWS Certified DevOps Engineer – Professional
    1. Microsoft
        1. Microsoft Certified Solutions Expert (MCSE) Private Cloud
        1. Microsoft Specialist certification in Office 365
        1. Microsoft Certified Specialist Developing Microsoft Azure Solution
        1. Microsoft Certified Specialist Implementing Microsoft Azure Infrastructure Solutions

## Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Edzo Botjes** - *Initial work* - [Gitlab](https://gitlab.com/edzob), [Blogger](http://blog.edzob.com/), [Medium](https://medium.com/@edzob), [LinkedIN](https://www.linkedin.com/in/edzob/), [twitter](https://twitter.com/edzob) 

See also the list of [contributors](https://gitlab.com/edzob/awesome-cloud-certifications/graphs/master) who participated in this project.

## License
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

```
#   You are free to:
#   Share — copy and redistribute the material in any medium or format.
#   Adapt — remix, transform, and build upon the material for any purpose, even commercially.
#   The licensor cannot revoke these freedoms as long as you follow the license terms.
#    
#    You can copy, modify, distribute and perform the work,  
#    even for commercial purposes, all without asking permission.
#    
#    Under the following terms:
#    Attribution — You must give appropriate credit, provide a link to the license, 
#                  and indicate if changes were made. You may do so in any reasonable manner, 
#                  but not in any way that suggests the licensor endorses you or your use.
#    ShareAlike — If you remix, transform, or build upon the material, 
#                 you must distribute your contributions under the same license as the original.
#    No additional restrictions — You may not apply legal terms or technological measures that 
#                 legally restrict others from doing anything the license permits.
#    
#    For more see the file 'LICENSE' for copying permission.
```

## Acknowledgments
### Inspiration on Awesomelist
* [The awesome manifesto by sindresorhus][awesome01]

[awesome01]: https://github.com/sindresorhus/awesome/blob/master/awesome.md

### Inspiration on Readme.md
* Inspiration on Readme.md
  * Github user PurpleBooth wrote: "[A template to make good README.md][readme01]"
  * Github user jxson wrote: "[README.md template][readme02]"
  * Medium user meakaakka wrote: "[A Beginners Guide to writing a Kickass README][readme03]"

### Inspiration on Markdown Syntax
* Inspiration on Markdown Syntax
    * GitHub   
      * Github user __adam-p__ wrote: "[Markdown Cheatsheet][markdown01]"
      * __Github Guides__ wrote: "[Markdown Syntax (pdf)][markdown02]"
      * __Github Guides__ wrote: "[Mastering Markdown (Wiki)][markdown03]"
      * Github user __tchapi__ wrote: "[Markdown Cheatsheet for Github][markdown04]"
    * GitLab
      * __GitLab documentation__ - Markdown wrote: "[GitLab Flavored Markdown (GFM)][markdown05]"
      * __GitLab Team Handbook__ - Markdown Guide wrote: "[Markdown kramdown Style Guide for about.GitLab.com][markdown06]"
      * __GitLab Markdown on Headers and IDs__ [Header IDs and links][markdown07]

[readme01]: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
[readme02]: https://gist.github.com/jxson/1784669
[readme03]: https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3

[markdown01]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[markdown02]: https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf
[markdown03]: https://guides.github.com/features/mastering-markdown/ 
[markdown04]: https://github.com/tchapi/markdown-cheatsheet

[markdown05]: https://docs.gitlab.com/ee/user/markdown.html
[markdown06]: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/
[markdown07]: https://docs.gitlab.com/ee/user/markdown.html#header-ids-and-links
